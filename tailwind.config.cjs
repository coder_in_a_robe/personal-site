const { tailwindExtractor } = require("tailwindcss/lib/lib/purgeUnusedStyles");

module.exports = {
    mode: "aot",
    purge: {
        content: ["./src/**/*.{html,js,svelte,ts}"],
        options: {
            defaultExtractor: (content) => [
                // If this stops working, please open an issue at https://github.com/svelte-add/tailwindcss/issues rather than bothering Tailwind Labs about it
                ...tailwindExtractor(content),
                // Match Svelte class: directives (https://github.com/tailwindlabs/tailwindcss/discussions/1731)
                ...[...content.matchAll(/(?:class:)*([\w\d-/:%.]+)/gm)].map(
                    ([_match, group, ..._rest]) => group
                ),
            ],
        },
        safelist: [/^svelte-[\d\w]+$/],
    },
    theme: {
        extend: {
            colors: {
                "darkazz-gray": "#293744",
                "philthy-white": "#EAEEF1",
                "da-blue": "#0085DF",
                charcoal: "#264653ff",
                "persian-green": "#2a9d8fff",
                "orange-yellow-crayola": "#e9c46aff",
                "sandy-brown": "#f4a261ff",
                "burnt-sienna": "#e76f51ff",
            },
        },
        fontFamily: {
            sans: ["Comfortaa", "cursive"],
            serif: ["Playfair Display", "serif"],
            mono: ["Roboto Mono", "monospace"],
            roboto: ["Roboto", "sans-serif"],
            hand: ["Nothing You Could Do", "cursive"],
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
};
